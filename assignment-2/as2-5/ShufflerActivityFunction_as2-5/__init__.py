# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging

logging.basicConfig(filename='example.log', encoding='utf-8', level=logging.DEBUG)


# list of k-v pairs
def main(input: list) -> set:
    res = dict()
    for l in input:
        for w in l:
            key = w[0]
            value = w[1]
            if key in res:
                res[key].append(value)
            else:
                res[key] = [value]
    return res
    
