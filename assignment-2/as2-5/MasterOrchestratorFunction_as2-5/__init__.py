import azure.durable_functions as df
import azure.functions as func
import requests


def orchestrator_function(context: df.DurableOrchestrationContext):
    files = ['https://as25blobs.blob.core.windows.net/files/mrinput-1.txt', 'https://as25blobs.blob.core.windows.net/files/mrinput-2.txt', 'https://as25blobs.blob.core.windows.net/files/mrinput-3.txt', 'https://as25blobs.blob.core.windows.net/files/mrinput-4F.txt']
    mapperResults = []
    reducerResults = []
    for file in files:
        req = requests.get(file)
        content = req.text
        for i, line in enumerate(content.splitlines()):
            tmp = yield context.call_activity('MapperActivityFunction_as2-5', [i, line.lower()])
            mapperResults.append(tmp)
    shufflerResult = yield context.call_activity('ShufflerActivityFunction_as2-5', mapperResults)
    for k, v in shufflerResult.items():
        tmp = yield context.call_activity('reducerActivityFunction_as2-5', [k, v])
        reducerResults.append(tmp)
    return reducerResults

main = df.Orchestrator.create(orchestrator_function)