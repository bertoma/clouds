# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging


# [key, [1,1,1,1,1...]]
def main(input: list) -> tuple:
    res = 0
    for e in input[1]:
        res = res + e
    return [input[0], res]
