from flask import Flask
from markupsafe import escape
import math

app = Flask(__name__)


@app.route('/<min>/<max>')
def NumericalIntegralEndpoint(min, max):
    # show the user profile for that user
    finalString = ""
    finalString += f'Min: {escape(min)}, Max: {escape(max)}<br>'

    for i in range(1, 7):
        finalString += f'{10 ** i}: ' + \
            str(computeNumericalIntegral(min, max, 10 ** i)) + "<br>"
    return finalString


def computeNumericalIntegral(min, max, intervals):
    result = 0
    workRange = float(max) - float(min)
    intervalLength = workRange / intervals
    for i in range(0, intervals):
        rectangleHeight = abs(math.sin(
            float(min) + (i * intervalLength) + (intervalLength / 2)))
        result += intervalLength * rectangleHeight
    return result
