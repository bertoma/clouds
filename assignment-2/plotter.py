import matplotlib.pyplot as plt
import pandas as pd


def plotter(datasets: list, labels: list, figsize=(15, 10)) -> None:
    plt.figure(figsize=figsize)
    for i, ds in enumerate(datasets):
        tss = []
        for t in ds["Timestamp"]:
            tss.append(t-ds["Timestamp"][0])
        plt.plot(tss, ds['Requests/s'], label=labels[i])
    plt.xlabel('Timestamp')
    plt.ylabel('Requests/s')
    plt.legend()
    plt.savefig('foo.png')
    

DS_PATHS = ['./csvs/fun/functions_stats_history.csv', './csvs/local/local_stats_history.csv', './csvs/webapp/webapp_stats_history.csv', './csvs/vm/vm_stats_history.csv']
    
    
if __name__ == '__main__':
    dataframes = []
    
    for path in DS_PATHS:
        df = pd.read_csv(path)
        dataframes.append(df)
    
    plotter(datasets=dataframes, labels=['functions', "local", "webapp", "vm"])
        