import logging
import math

import azure.functions as func


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    min = req.params.get('min')
    max = req.params.get('max')

    if min is not None and max is not None:
        finalString = ""
        finalString += f'Min: {min}, Max: {max}\n'

        for i in range(1, 7):
            finalString += f'{10 ** i}: ' + \
                str(computeNumericalIntegral(min, max, 10 ** i)) + "\n"
        return func.HttpResponse(finalString)
    else:
        return func.HttpResponse(
             "pass max and min as get parameters",
             status_code=200
        )
        
def computeNumericalIntegral(min, max, intervals):
    result = 0
    workRange = float(max) - float(min)
    intervalLength = workRange / intervals
    for i in range(0, intervals):
        rectangleHeight = abs(math.sin(
            float(min) + (i * intervalLength) + (intervalLength / 2)))
        result += intervalLength * rectangleHeight
    return result
